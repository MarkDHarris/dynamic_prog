use dynamic_prog::prng::Prng;
use std::time::Instant;

// 1. Exhaustive search

const NUM_ITEMS: i32 = 23; // A reasonable value for exhaustive search.

const MIN_VALUE: i32 = 1;
const MAX_VALUE: i32 = 10;
const MIN_WEIGHT: i32 = 4;
const MAX_WEIGHT: i32 = 10;

struct Item {
    id: i32,
    value: i32,
    weight: i32,
    is_selected: bool,
    blocked_by: i32,
    block_list: Vec<i32>,
}

fn main() {
    // Prepare a Prng using the same seed each time.
    let mut prng = Prng { seed: 1337 };

    //prng.randomize();

    // Make some random items.
    let mut items = make_items(
        &mut prng, NUM_ITEMS, MIN_VALUE, MAX_VALUE, MIN_WEIGHT, MAX_WEIGHT,
    );
    let allowed_weight = sum_weights(&mut items, true) / 2;

    // Display basic parameters.
    println!("*** Parameters ***");
    println!("# items:        {}", NUM_ITEMS);
    println!("Total value:    {}", sum_values(&mut items, true));
    println!("Total weight:   {}", sum_weights(&mut items, true));
    println!("Allowed weight: {}", allowed_weight);
    println!();

    // Exhaustive search
    if NUM_ITEMS > 23 {
        // Only run exhaustive search if num_items is small enough.
        println!("Too many items for exhaustive search\n");
    } else {
        println!("*** Exhaustive Search ***");
        run_algorithm(&exhaustive_search, &mut items, allowed_weight);
    }

    // Branch and bound
    if NUM_ITEMS > 40 {
        // Only run branch and bound if num_items is small enough.
        println!("Too many items for branch and bound\n");
    } else {
        println!("*** Branch and Bound ***");
        run_algorithm(&branch_and_bound, &mut items, allowed_weight);
    }

    // Rod's technique
    if NUM_ITEMS > 60 {
        // Only run Rod's technique if num_items is small enough.
        println!("Too many items for Rod's technique\n");
    } else {
        println!("*** Rod's Technique ***");
        run_algorithm(&rods_technique, &mut items, allowed_weight);
    }

    // Rod's technique sorted
    if NUM_ITEMS > 250 {
        // Only run Rod's technique sorted if num_items is small enough.
        println!("Too many items for Rod's technique sorted");
    } else {
        println!("*** Rod's Technique Sorted ***");
        run_algorithm(&rods_technique_sorted, &mut items, allowed_weight);
    }

    // Dynamic programming
    println!("*** Dynamic programming ***");
    run_algorithm(&dynamic_programming, &mut items, allowed_weight);
}

// Make some random items.
fn make_items(
    prng: &mut Prng,
    num_items: i32,
    min_value: i32,
    max_value: i32,
    min_weight: i32,
    max_weight: i32,
) -> Vec<Item> {
    let mut items: Vec<Item> = Vec::with_capacity(num_items as usize);
    for i in 0..num_items {
        let item = Item {
            id: i,
            value: prng.next_i32(min_value, max_value),
            weight: prng.next_i32(min_weight, max_weight),
            is_selected: false,
            blocked_by: -1,
            block_list: Vec::new(),
        };
        items.push(item);
    }
    return items;
}

fn make_block_lists(items: &mut Vec<Item>) {
    for i in 0..items.len() {
        items[i].block_list = Vec::new();

        for j in 0..items.len() {
            if i != j {
                if items[i].value >= items[j].value && items[i].weight <= items[j].weight {
                    let id = items[i].id;
                    items[i].block_list.push(id);
                }
            }
        }
    }
}

// Return a copy of the items.
fn copy_items(items: &mut Vec<Item>) -> Vec<Item> {
    let mut new_items: Vec<Item> = Vec::with_capacity(items.len());
    for item in items {
        let new_item = Item {
            id: item.id,
            value: item.value,
            weight: item.weight,
            is_selected: item.is_selected,
            blocked_by: item.blocked_by,
            block_list: item.block_list.clone(),
        };
        new_items.push(new_item);
    }
    return new_items;
}

// Return the total value of the items.
// If add_all is false, only add up the selected items.
fn sum_values(items: &mut Vec<Item>, add_all: bool) -> i32 {
    let mut total = 0;
    for i in 0..items.len() {
        if add_all || items[i].is_selected {
            total += items[i].value;
        }
    }
    return total;
}

// Return the total weight of the items.
// If add_all is false, only add up the selected items.
fn sum_weights(items: &mut Vec<Item>, add_all: bool) -> i32 {
    let mut total = 0;
    for i in 0..items.len() {
        if add_all || items[i].is_selected {
            total += items[i].weight;
        }
    }
    return total;
}

// Return the value of this solution.
// If the solution is too heavy, return -1 so we prefer an empty solution.
fn solution_value(items: &mut Vec<Item>, allowed_weight: i32) -> i32 {
    // If the solution's total weight > allowed_weight,
    // return 0 so we won't use this solution.
    if sum_weights(items, false) > allowed_weight {
        return -1;
    }

    // Return the sum of the selected values.
    return sum_values(items, false);
}

// Print the selected items.
fn print_selected(items: &mut Vec<Item>) {
    let mut num_printed = 0;
    for i in 0..items.len() {
        if items[i].is_selected {
            print!("{}({}, {}) ", i, items[i].value, items[i].weight)
        }
        num_printed += 1;
        if num_printed > 100 {
            println!("...");
            return;
        }
    }
    println!();
}

// Run the algorithm. Display the elapsed time and solution.
fn run_algorithm(
    alg: &dyn Fn(&mut Vec<Item>, i32) -> (Vec<Item>, i32, i32),
    items: &mut Vec<Item>,
    allowed_weight: i32,
) {
    // Copy the items so the run isn't influenced by a previous run.
    let mut test_items = copy_items(items);

    let start = Instant::now();

    // Run the algorithm.
    let mut solution: Vec<Item>;
    let total_value: i32;
    let function_calls: i32;
    (solution, total_value, function_calls) = alg(&mut test_items, allowed_weight);

    let duration = start.elapsed();
    println!("Elapsed: {:?}", duration);

    print_selected(&mut solution);
    println!(
        "Value: {}, Weight: {}, Calls: {}",
        total_value,
        sum_weights(&mut solution, false),
        function_calls
    );
    println!();
}

// Recursively assign values in or out of the solution.
// Return the best assignment, value of that assignment,
// and the number of function calls we made.
fn exhaustive_search(items: &mut Vec<Item>, allowed_weight: i32) -> (Vec<Item>, i32, i32) {
    return do_exhaustive_search(items, allowed_weight, 0);
}

fn do_exhaustive_search(
    items: &mut Vec<Item>,
    allowed_weight: i32,
    next_index: i32,
) -> (Vec<Item>, i32, i32) {
    // See if we have a full assignment.
    if next_index >= items.len() as i32 {
        // Make a copy of this assignment.
        let mut assignment = copy_items(items);

        // Return the assignment and its total value.
        let value = solution_value(&mut assignment, allowed_weight);
        return (assignment, value, 1);
    }

    // We do not have a full assignment.
    // Try adding the next item.
    items[next_index as usize].is_selected = true;

    // Recursively call the function.
    let test1_solution: Vec<Item>;
    let test1_value: i32;
    let test1_calls: i32;
    (test1_solution, test1_value, test1_calls) =
        do_exhaustive_search(items, allowed_weight, next_index + 1);

    // Try not adding the next item.
    items[next_index as usize].is_selected = false;

    // Recursively call the function.
    let test2_solution: Vec<Item>;
    let test2_value: i32;
    let test2_calls: i32;
    (test2_solution, test2_value, test2_calls) =
        do_exhaustive_search(items, allowed_weight, next_index + 1);

    // Return the solution that is better.
    if test1_value >= test2_value {
        return (test1_solution, test1_value, test1_calls + test2_calls + 1);
    } else {
        return (test2_solution, test2_value, test1_calls + test2_calls + 1);
    }
}

// Use branch and bound to find a solution.
// Return the best assignment, value of that assignment,
// and the number of function calls we made.
fn branch_and_bound(items: &mut Vec<Item>, allowed_weight: i32) -> (Vec<Item>, i32, i32) {
    let best_value = 0;
    let current_value = 0;
    let current_weight = 0;
    let remaining_value = sum_values(items, true);

    return do_branch_and_bound(
        items,
        allowed_weight,
        0,
        best_value,
        current_value,
        current_weight,
        remaining_value,
    );
}

fn do_branch_and_bound(
    items: &mut Vec<Item>,
    allowed_weight: i32,
    next_index: usize,
    mut best_value: i32,
    current_value: i32,
    current_weight: i32,
    remaining_value: i32,
) -> (Vec<Item>, i32, i32) {
    // See if we have a full assignment.
    if next_index >= items.len() {
        // Make a copy of this assignment.
        let solution = copy_items(items);

        // Return the assignment and its total value.
        return (solution, current_value, 1);
    }

    // We do not have a full assignment.
    // See if we can improve this solution enough to be worth persuing.
    if current_value + remaining_value <= best_value {
        // We cannot improve on the best solution found so far.
        return (Vec::new(), 0, 1);
    }

    // Try adding the next item.
    let test1_solution: Vec<Item>;
    let test1_value: i32;
    let test1_calls: i32;
    if current_weight + items[next_index].weight <= allowed_weight {
        items[next_index].is_selected = true;
        (test1_solution, test1_value, test1_calls) = do_branch_and_bound(
            items,
            allowed_weight,
            next_index + 1,
            best_value,
            current_value + items[next_index].value,
            current_weight + items[next_index].weight,
            remaining_value - items[next_index].value,
        );
        if test1_value > best_value {
            best_value = test1_value;
        }
    } else {
        test1_solution = Vec::new();
        test1_value = 0;
        test1_calls = 1;
    }

    // Try not adding the next item.
    let test2_solution: Vec<Item>;
    let test2_value: i32;
    let test2_calls: i32;
    // See if there is a chance of improvement without this item's value.
    if current_value + remaining_value - items[next_index].value > best_value {
        items[next_index].is_selected = false;
        (test2_solution, test2_value, test2_calls) = do_branch_and_bound(
            items,
            allowed_weight,
            next_index + 1,
            best_value,
            current_value,
            current_weight,
            remaining_value - items[next_index].value,
        );
    } else {
        test2_solution = Vec::new();
        test2_value = 0;
        test2_calls = 1;
    }

    // Return the solution that is better.
    if test1_value >= test2_value {
        return (test1_solution, test1_value, test1_calls + test2_calls + 1);
    } else {
        return (test2_solution, test2_value, test1_calls + test2_calls + 1);
    }
}

// Use Rod's technique to find a solution.
// Return the best assignment, value of that assignment,
// and the number of function calls we made.
fn rods_technique(items: &mut Vec<Item>, allowed_weight: i32) -> (Vec<Item>, i32, i32) {
    // Make the block lists.
    make_block_lists(items);

    // Initialize parameters.
    let best_value = 0;
    let current_value = 0;
    let current_weight = 0;
    let remaining_value = sum_values(items, true);

    // Call do_rods_technique.
    return do_rods_technique(
        items,
        allowed_weight,
        0,
        best_value,
        current_value,
        current_weight,
        remaining_value,
    );
}

// Use Rod's technique sorted to find a solution.
// Return the best assignment, value of that assignment,
// and the number of function calls we made.
fn rods_technique_sorted(items: &mut Vec<Item>, allowed_weight: i32) -> (Vec<Item>, i32, i32) {
    // Make initial block lists.
    make_block_lists(items);

    // Sort so items with longer blocked lists come first.
    items.sort_by(|a, b| b.block_list.len().cmp(&a.block_list.len()));

    // Reset the items' IDs.
    for i in 0..items.len() {
        items[i].id = i as i32;
    }

    // Rebuild the blocked lists with the new indices.
    make_block_lists(items);

    // Initialize parameters.
    let best_value = 0;
    let current_value = 0;
    let current_weight = 0;
    let remaining_value = sum_values(items, true);

    // Call do_rods_technique.
    return do_rods_technique(
        items,
        allowed_weight,
        0,
        best_value,
        current_value,
        current_weight,
        remaining_value,
    );
}

fn do_rods_technique(
    items: &mut Vec<Item>,
    allowed_weight: i32,
    next_index: usize,
    mut best_value: i32,
    current_value: i32,
    current_weight: i32,
    remaining_value: i32,
) -> (Vec<Item>, i32, i32) {
    // See if we have a full assignment.
    if next_index >= items.len() {
        // Make a copy of this assignment.
        let solution = copy_items(items);

        // Return the assignment and its total value.
        return (solution, current_value, 1);
    }

    // We do not have a full assignment.
    // See if we can improve this solution enough to be worth persuing.
    if current_value + remaining_value <= best_value {
        // We cannot improve on the best solution found so far.
        return (Vec::new(), current_value, 1);
    }

    // Try adding the next item.
    let mut test1_solution = Vec::new();
    let mut test1_value = 0;
    let mut test1_calls = 1;

    // See if the item is blocked.
    if items[next_index].blocked_by < 0 {
        // See if adding this item might lead to an improvement.
        if current_weight + items[next_index].weight <= allowed_weight {
            // Try adding the item.
            items[next_index].is_selected = true;
            (test1_solution, test1_value, test1_calls) = do_rods_technique(
                items,
                allowed_weight,
                next_index + 1,
                best_value,
                current_value + items[next_index].value,
                current_weight + items[next_index].weight,
                remaining_value - items[next_index].value,
            );
            if test1_value > best_value {
                best_value = test1_value;
            }
        }
    }

    // Try not adding the next item.
    let test2_solution: Vec<Item>;
    let test2_value: i32;
    let test2_calls: i32;
    // See if there is a chance of improvement without this item's value.
    if current_value + remaining_value - items[next_index].value > best_value {
        // Block items on this item's blocks list.
        // Copy the block list.
        let current_block_list = items[next_index].block_list.to_vec();

        // Place the blocks.
        for i in 0..current_block_list.len() {
            let other_id = current_block_list[i];
            let other_index = other_id as usize;
            let ref other = &items[other_index];
            if other.blocked_by < 0 {
                items[other_index].blocked_by = items[next_index].id;
            }
        }

        items[next_index].is_selected = false;
        (test2_solution, test2_value, test2_calls) = do_rods_technique(
            items,
            allowed_weight,
            next_index + 1,
            best_value,
            current_value,
            current_weight,
            remaining_value - items[next_index].value,
        );

        // Remove the blocks.
        for i in 0..current_block_list.len() {
            let other_id = current_block_list[i];
            let other_index = other_id as usize;
            let ref other = &items[other_index];
            if other.blocked_by == items[next_index].id {
                items[other_index].blocked_by = -1;
            }
        }
    } else {
        test2_solution = Vec::new();
        test2_value = 0;
        test2_calls = 1;
    }

    // Return the solution that is better.
    if test1_value >= test2_value {
        return (test1_solution, test1_value, test1_calls + test2_calls + 1);
    } else {
        return (test2_solution, test2_value, test1_calls + test2_calls + 1);
    }
}

// Use dynamic programming to find a solution.
// Return the best assignment, value of that assignment,
// and the number of function calls we made.
fn dynamic_programming(items: &mut Vec<Item>, allowed_weight: i32) -> (Vec<Item>, i32, i32) {
    // solution[i][w] represents the best solution for
    // items 0 through i and weight w.
    // We don't actually store solution[i][w]; we reconstruct
    // the solution at the end of the function.
    //
    // solution_value[i][w] holds the value of solution[i][w].
    //
    // prev_weight[i][w] holds the index j where we used solution[j][w - 1]
    // to get to solution[i][w].
    //
    // Note that we need to handle solutions where w is between 0 and allowed_weight,
    // so w must be able to take on allowed_weight + 1 values.

    // Allocate the arrays.
    let mut solution_value: Vec<Vec<i32>> = Vec::with_capacity(NUM_ITEMS as usize);
    let mut prev_weight: Vec<Vec<i32>> = Vec::with_capacity(NUM_ITEMS as usize);
    for _i in 0..NUM_ITEMS {
        solution_value.push(vec![0; (allowed_weight + 1) as usize]);
        prev_weight.push(vec![0; (allowed_weight + 1) as usize]);
    }

    // Initialize the row item 0.
    for w in 0..=allowed_weight {
        if items[0].weight <= w {
            // items[0] fits.
            solution_value[0][w as usize] = items[0].value;
            prev_weight[0][w as usize] = -1;
        } else {
            // items[0] does not fit.
            solution_value[0][w as usize] = 0;
            prev_weight[0][w as usize] = w;
        }
    }

    // Fill in the remaining table rows.
    for i in 1..NUM_ITEMS as usize {
        for w in 0..=allowed_weight as usize {
            // Calculate the value if we do not use the new item i.
            let value_without_i = solution_value[i - 1][w];

            // Calculate the value if we do use the new item i.
            let mut value_with_i = 0;
            if items[i].weight <= w as i32 {
                // Make sure it fits.
                value_with_i = solution_value[i - 1][w - items[i].weight as usize] + items[i].value;
            }

            // See which is better.
            if value_without_i >= value_with_i {
                // We're better off omitting item i.
                solution_value[i][w] = value_without_i;
                prev_weight[i][w] = w as i32;
            } else {
                // We're better off including item i.
                solution_value[i][w] = value_with_i;
                prev_weight[i][w] = w as i32 - items[i].weight;
            }
        }
    }

    // Set is_selected to false for all items.
    // (It should already be false, but let's play it safe.)
    for i in 0..NUM_ITEMS as usize {
        items[i].is_selected = false;
    }

    // Reconstruct the solution.
    // Get the row and column for the final solution.
    let mut back_i = NUM_ITEMS - 1;
    let mut back_w = allowed_weight;

    // Work backwards until we reach an initial solution.
    while back_i >= 0 {
        // Check prev_weight for the current solution.
        let prev_w = prev_weight[back_i as usize][back_w as usize];
        if back_w == prev_w {
            // We skipped item back_i.
            // Leave back_w unchanged.
        } else {
            // We added item back_i.
            items[back_i as usize].is_selected = true; // Select this item in the solution.
            back_w = prev_w; // Move to the previous solution's weight.
        }
        back_i -= 1; // Move to the previous row.
    }

    // Return a copy of the items.
    let solution = copy_items(items);
    return (
        solution,
        solution_value[(NUM_ITEMS - 1) as usize][allowed_weight as usize],
        1,
    );
}
